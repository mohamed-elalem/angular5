import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';
import { ShoppingListService } from '../../services/shopping-list.service';
import { IngredientForm } from '../../forms/ingredient.form';
import { FormGroup, NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  @ViewChild('ingredientName') ingredientName: ElementRef;
  @ViewChild('ingredientQuantity') ingredientQuantity: ElementRef;
  @Output() newIngredient: EventEmitter<Ingredient> = new EventEmitter<Ingredient>();
  // ingredientForm: FormGroup;
  @ViewChild('form') private ingredientForm: NgForm;
  subscription: Subscription;
  editMode: boolean = false;
  editItemIndex: number;
  editedItem: Ingredient;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    // this.ingredientForm = new IngredientForm().getForm();
    this.subscription = this.shoppingListService.notifyEditIngredient.subscribe(
      (index: number) => {
        this.editMode = true;
        this.editItemIndex = index;
        this.editedItem = this.shoppingListService.getIngredient(index);
        this.ingredientForm.setValue({
          name: this.editedItem.name,
          amount: this.editedItem.amount
        });
      }
    );
  }

  public addIngredient() {
    // this.newIngredient.emit(new Ingredient(this.ingredientName.nativeElement.value, this.ingredientQuantity.nativeElement.value));
    const newIngredient: Ingredient = new Ingredient(
      this.ingredientName.nativeElement.value,
      this.ingredientQuantity.nativeElement.value
    );
    if (this.editMode) {
      this.editIngredient(this.editItemIndex, newIngredient);
    } else {
      this.shoppingListService.addToIngredients(newIngredient);
    }
    this.shoppingListService.notifyIngredientChange.next();
  }

  onSubmit() {
    const newIngredient: Ingredient = new Ingredient(
      this.ingredientForm.value.name,
      this.ingredientForm.value.amount
    );
    if (this.editMode) {
      this.editIngredient(this.editItemIndex, newIngredient);
    } else {
      this.shoppingListService.addToIngredients(newIngredient);
    }
    this.shoppingListService.notifyIngredientChange.next();

    this.onClear();
  }

  onDelete() {
    this.shoppingListService.removeIngredient(this.editItemIndex);
    this.shoppingListService.notifyIngredientChange.next();
    this.onClear();
  }

  onClear() {
    this.ingredientForm.reset({
      amount: 0
    });


    this.editMode = false;
    this.editedItem = null;
    this.editItemIndex = null;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  editIngredient(index: number, newIngredient: Ingredient) {

    this.shoppingListService.replaceIngredient(index, newIngredient);
    this.editedItem = newIngredient;
  }
}
