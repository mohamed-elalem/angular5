import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../services/shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
})
export class ShoppingListComponent implements OnInit {
  @ViewChild('ingredientName') ingredientName: ElementRef;
  @ViewChild('ingredientQuantity') ingredientQuantity: ElementRef;
  ingredients: Ingredient[];
  //  = [
  //   new Ingredient('Apples', 5),
  //   new Ingredient('Tomatoes', 10),
  // ];

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.ingredients = this.shoppingListService.getIngredients();
    this.shoppingListService.notifyIngredientChange.subscribe(
      () => {
        this.ingredients = this.shoppingListService.getIngredients();
      }
    );
  }

  // public addNewIngredient(ingredient: Ingredient) {
  //   this.ingredients.push(ingredient);
  // }

  onEditIngredient(index: number) {
    this.shoppingListService.notifyEditIngredient.next(index);
  }

}
