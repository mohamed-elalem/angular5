import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { RecipeService } from '../../services/recipe.service';
import { Recipe } from '../../recipes/recipe.model';

@Injectable()
export class RecipeResolverService implements Resolve<Recipe> {

  public constructor(private recipeService: RecipeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipe> | Promise<Recipe> | Recipe {
    return this.recipeService.getRecipe(route.params['id']);
  }
}
