import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { RecipeService } from '../../services/recipe.service';
import { RecipeForm } from '../../forms/recipe.form';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { Recipe } from '../recipe.model';
import { IngredientForm } from '../../forms/ingredient.form';
import { Ingredient } from '../../shared/ingredient.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: number;
  editMode: boolean;
  recipeForm: FormGroup;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        console.log(this.editMode);
      }
    );

    this.recipeForm = new RecipeForm().getForm();
  }

  public onAddIngredient() {
    (<FormArray>this.recipeForm.get('ingredients')).push(new IngredientForm().getForm());
  }

  public onSubmit() {
    const ingredients: Ingredient[] = new Array<Ingredient>();
    console.log((<FormGroup>this.recipeForm.get('ingredients')).controls);
    for (let ingredientForm: FormGroup of (<FormGroup>this.recipeForm.get('ingredients')).controls) {
      ingredients.push(new Ingredient(ingredientForm.get('name').value, ingredientForm.get('amount').value));
    }
    console.log(ingredients);
    const recipe: Recipe = new Recipe(
      this.recipeForm.get('info.name').value,
      this.recipeForm.get('info.description').value,
      this.recipeForm.get('info.imagePath').value,
      ingredients
    );
    this.recipeService.addNewRecipe(recipe);
    this.recipeService.notifyRecipeChanged.next();
  }

}
