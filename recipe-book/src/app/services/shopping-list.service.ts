import { EventEmitter } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Subject } from 'rxjs/Subject';

export class ShoppingListService {
  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10),
  ];
  public notifyIngredientChange: Subject<void> = new Subject<void>();
  public notifyEditIngredient: Subject<number> = new Subject<number>();

  public getIngredients(): Ingredient[] {
    return this.ingredients.slice();
  }

  public getIngredient(index: number): Ingredient {
    return this.ingredients[index];
  }

  public replaceIngredient(index: number, newIngredient: Ingredient) {
    this.ingredients[index] = newIngredient;
  }

  public addToIngredients(ingredient: Ingredient): void {
    this.ingredients.push(ingredient);
  }

  public addListOfIngredients(ingredients: Ingredient[]): void {
    this.ingredients.push(...ingredients);
  }

  public removeIngredient(index: number) {
    this.ingredients.splice(index, 1);
  }
}
