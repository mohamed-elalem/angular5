import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
export class RecipeForm {
  private form: FormGroup;

  public constructor() {
    this.form = new FormGroup({
      info: new FormGroup({
        name: new FormControl(null),
        description: new FormControl(null),
        imagePath: new FormControl(null)
      }),
      ingredients: new FormArray([])
    });
  }

  public getForm() {
    return this.form;
  }
}
