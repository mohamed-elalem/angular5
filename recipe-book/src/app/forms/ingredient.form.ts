import { FormGroup, FormControl, Validators } from '@angular/forms';
export class IngredientForm {
  private form: FormGroup;

  public constructor() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      amount: new FormControl(0, [Validators.required, Validators.min(0)])
    });
  }

  public getForm(): FormGroup {
    return this.form;
  }
}
