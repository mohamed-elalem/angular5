import {
  Directive,
  OnInit,
  Renderer2,
  ElementRef,
  HostListener,
  HostBinding
} from '@angular/core';

@Directive({
  selector: '[appDropDown]'
})

export class DropDownDirective implements OnInit {
  @HostBinding('class.open') private drop: boolean;
  public constructor(private renderer: Renderer2, private elementRef: ElementRef) {}

  ngOnInit() {
    this.drop = false;
  }

  @HostListener('click') public onClick(event: Event) {
    this.drop = !this.drop;
    // if (this.drop) {
    //   this.renderer.addClass(this.elementRef.nativeElement, 'open');
    // } else {
    //   this.renderer.removeClass(this.elementRef.nativeElement, 'open');
    // }
  }
}
