import { NgForm } from '@angular/forms';
import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f') private form: NgForm;
  public defaultQuestion: string = 'pet';
  public questionAnswer: string = '';
  public genders: string[] = ['Male', 'Female'];
  public isSubmitted: boolean = false;
  public user: {
    username: string,
    email: string,
    question: string,
    answer: string,
    gender: string,
  };

  suggestUserName() {
    const suggestedName = 'Superuser';
    this.form.form.patchValue(
      {
        userData: {
          username: suggestedName
        }
      }
    );
  }

  // public onSubmit(form: NgForm) {
  //   console.log(form);
  // }

  public onSubmit() {
    console.log(this.form);
    this.isSubmitted = true;
    this.user = {
      username: this.form.value.userData.username,
      email: this.form.value.userData.email,
      question: this.form.value.secret,
      answer: this.form.value.questionAnswer,
      gender: this.form.value.gender
    };
    // this.user.username = this.form.value.userData.username;
    // this.user.email = this.form.value.userData.email;
    // this.user.question = this.form.value.secret;
    // this.user.answer = this.form.value.questionAnswer;
    // this.user.gender = this.form.value.gender;

    this.form.reset();
  }
}
