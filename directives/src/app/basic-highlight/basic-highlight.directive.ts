import { Directive, OnInit, ElementRef } from '@angular/core';

@Directive({
  'selector': '[appBasicHighlight]',
})

export class BasicHighlightDirective implements OnInit {

  public constructor(private elementRef: ElementRef) {
    console.log('There');
  }

  ngOnInit() {
    console.log('highlighting');
    this.elementRef.nativeElement.style.backgroundColor = 'green';
  }
}
