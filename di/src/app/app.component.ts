import { Component, OnInit } from '@angular/core';
import { AccountService } from './services/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  // providers: [
  //   AccountService
  // ]
})
export class AppComponent implements OnInit {
  // accounts = [
  //   {
  //     name: 'Master Account',
  //     status: 'active'
  //   },
  //   {
  //     name: 'Testaccount',
  //     status: 'inactive'
  //   },
  //   {
  //     name: 'Hidden Account',
  //     status: 'unknown'
  //   }
  // ];

  // onAccountAdded(newAccount: {name: string, status: string}) {
  //   this.accounts.push(newAccount);
  // }

  // onStatusChanged(updateInfo: {id: number, newStatus: string}) {
  //   this.accounts[updateInfo.id].status = updateInfo.newStatus;
  // }

  private accounts: {name: string, status: string}[];

  public ngOnInit() {
    this.accounts = this.accountService.accounts;
  }

  public constructor(private accountService: AccountService) {}

  public onAccountAdded(newAccount: {name: string, status: string}) {
    this.accountService.onAccountAdded(newAccount);
  }

  public onStatusChanged(updateInfo: {id: number, newStatus: string}) {
    this.accountService.onStatusChanged(updateInfo);
  }
}
