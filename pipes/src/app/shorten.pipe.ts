import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: string, length: number = 10): string {
    let newValue = value;
    if (value.length > length) {
      newValue = value.substr(0, length) + ' ...';
    }
    return newValue;
  }
}
