import { Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {
  public transform(value: any, filterText: string, filterPropertry: string) {
    if (filterText == null || filterText.length === 0) {
      return value;
    }
    const resArray: Array<any> = [];
    for (const item of value) {
      if (item.status.indexOf(filterText) > -1) {
        resArray.push(item);
      }
    }
    return resArray;
  }
}
