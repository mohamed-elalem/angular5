import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/Rx';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private interval: Subscription;
  private packages: Subscription;

  constructor() {}

  ngOnInit() {
    const myNumbers = Observable.interval(1000);
    this.interval = myNumbers.subscribe(
      (number: number) => {
        console.log(number);
      }
    );
    const myObservable: Observable<string> = Observable.create(
      (observer: Observer<string>) => {
        setTimeout(() => {
          observer.next('first package');
        }, 2000);
        setTimeout(() => {
          observer.next('second package');
        }, 4000);
        setTimeout(() => {
          // Incase of an error completed won't be called
          observer.error('first error');
        }, 6000);
        setTimeout(() => {
          observer.complete();
        }, 8000);
        setTimeout(() => {
          observer.next('third package');
        }, 10000);
      }
    );
    this.packages = myObservable.subscribe(
      (res: string) => {
        console.log(res);
      },
      (err: string) => {
        console.log(err);
      },
      () => {
        console.log('completed!!');
      },
    );
  }

  ngOnDestroy() {
    this.interval.unsubscribe();
    this.packages.unsubscribe();
  }
}
