import { UserService } from './services/user-service.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    UserService
  ]
})
export class AppComponent implements OnInit, OnDestroy {
  public activated: string[];

  public constructor(private userService: UserService) {}

  ngOnInit() {
    this.activated = new Array<string>(3);

    this.userService.userSubject.subscribe(
      (id: number) => {
        this.activated[id] = '(activated)';
      }
    );
  }

  ngOnDestroy() {
    this.userService.userSubject.unsubscribe();
  }
}
