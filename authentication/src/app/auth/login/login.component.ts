import { Component, OnInit, ViewChild} from '@angular/core';
import { AuthService } from '../auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  @ViewChild("form") loginForm: NgForm;

  public constructor(private authService: AuthService) {}

  ngOnInit() {}

  public onSubmit() {
    const email: string = this.loginForm.value.email;
    const password: string = this.loginForm.value.password;
    this.authService.login(email, password);
  }
}
