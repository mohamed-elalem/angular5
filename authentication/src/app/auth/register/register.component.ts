import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    './register.component.css'
  ]
})
export class RegisterComponent implements OnInit {
  @ViewChild('form') registrationForm: NgForm;


  public constructor(private authService: AuthService) {}

  public ngOnInit() {}

  public onSubmit() {
    const email: string = this.registrationForm.value.email;
    const password: string = this.registrationForm.value.password;
    this.authService.register(email, password);
  }
}
