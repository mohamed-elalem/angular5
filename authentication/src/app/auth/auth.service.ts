import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Injectable()
export class AuthService {

  private token: string;

  public constructor(private router: Router) {}

  register(email: string, password: string) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(
      (response) => console.log(response)
    ).then(
      (error) => console.error(error)
    ).catch(
      (exp) => console.error(exp)
    );
  }

  login(email: string, password: string) {
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(
      (response) => {
        firebase.auth().currentUser.getToken()
        .then(
          (token: string) => {
            console.log(token);
            this.token = token;
            this.router.navigate(['']);
          }
        );
      }
    ).catch(
      (error) => console.log(error)
    );
  }

  getToken() {
    firebase.auth().currentUser.getToken()
    .then(
      (token) => {
        this.token = token;
      }
    );
    return this.token;
  }

  isAuthenticated() {
    return this.token != null;
  }

  logout() {
    firebase.auth().signOut();
    this.token = null;
  }
}
