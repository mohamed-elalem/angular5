export class AuthService {
  private isLoggedIn: boolean;

  constructor() {
    this.isLoggedIn = false;
  }

  public login() {
    this.isLoggedIn = true;
  }

  public logout() {
    this.isLoggedIn = false;
  }

  public isAuthenticated(): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        setTimeout(
          () => {
            resolve(this.isLoggedIn);
          },
          800
        );
      }
    );
  }
}
