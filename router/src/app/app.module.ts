import { AuthService } from './auth.service';
import { AuthGaurd } from './auth-guard.service';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { ServersComponent } from './servers/servers.component';
import { UserComponent } from './users/user/user.component';
import { EditServerComponent } from './servers/edit-server/edit-server.component';
import { ServerComponent } from './servers/server/server.component';
import { ServersService } from './servers/servers.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { CanDeactivateGaurd } from './can-deactivate.service';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ServerResolver } from './servers/server/server-resolver.service';

// const appRoutes = [
//   {
//     path: '',
//     component: HomeComponent
//   },
//   {
//     path: 'users',
//     component: UsersComponent,
//     children: [
//       {
//         path: ':id/:name',
//         component: UserComponent
//       }
//     ]
//   },
//   {
//     path: 'servers',
//     component: ServersComponent,
//     children: [
//       {
//         path: ':id',
//         component: ServerComponent
//       },
//       {
//         path: ':id/edit',
//         component: EditServerComponent
//       }
//     ]
//   },
//   {
//     path: 'not-found',
//     component: NotFoundComponent
//   },
//   {
//     path: '**',
//     redirectTo: 'not-found'
//   }
// ];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UsersComponent,
    ServersComponent,
    UserComponent,
    EditServerComponent,
    ServerComponent,
    NotFoundComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    // RouterModule.forRoot(appRoutes),
    AppRoutingModule
  ],
  providers: [
    ServersService,
    AuthGaurd,
    AuthService,
    CanDeactivateGaurd,
    ServerResolver,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
