import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { CanActivateChild } from '@angular/router/src/interfaces';


@Injectable()
export class AuthGaurd implements CanActivate, CanActivateChild {

  public constructor(private authService: AuthService, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.isAuthenticated().
    then(
      (authenticated: boolean) => {
        switch (authenticated) {
          case true:
            return true;
          default:
            this.router.navigate(['/']);
            return false;
        }
      }
    );
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }
}
