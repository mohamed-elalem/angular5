import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  genders = ['male', 'female'];
  forbiddenNames = ['Chris', 'Anna'];
  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      userData: new FormGroup({
        username: new FormControl(null, [Validators.required, this.forbiddenUsernames.bind(this)]),
        email: new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails)
      }),
      gender: new FormControl('male'),
      hobbies: new FormArray([]),
    });

    // this.form.valueChanges.subscribe(
    //   (value) => {
    //     console.log(value);
    //   }
    // );

    this.form.statusChanges.subscribe(
      (status) => {
        console.log(status);
      }
    );
  }

  onSubmit() {
    console.log(this.form);
  }

  onAddHobby() {
    (<FormArray>this.form.get('hobbies')).push(new FormControl(null, Validators.required));
  }

  forbiddenUsernames(control: FormControl): {[s: string]: boolean} {
    for (let forbiddenName of this.forbiddenNames) {
      if (control.value != null && control.value.length > 0 && forbiddenName.indexOf(control.value) > -1) {
        return {
          'forbiddenName': true
        };
      }
    }
    return null;
  }

  forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          if (control.value === 'test@test.com') {
            resolve({
              'forbiddenEmail': true
            });
          } else {
            resolve(null);
          }
        }, 1500);
      });
    return promise;
  }
}
