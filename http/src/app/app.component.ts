import { ServerService } from './service/server-service.service';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  appName;

  public constructor(private serverService: ServerService) {}

  servers = [
    {
      name: 'Testserver',
      capacity: 10,
      id: this.generateId()
    },
    {
      name: 'Liveserver',
      capacity: 100,
      id: this.generateId()
    }
  ];
  onAddServer(name: string) {
    this.servers.push({
      name: name,
      capacity: 50,
      id: this.generateId()
    });
  }

  ngOnInit() {
    this.appName = this.serverService.getAppName();
  }

  private generateId() {
    return Math.round(Math.random() * 10000);
  }

  public addServers() {
    this.serverService.addServers(this.servers).subscribe(
      (response) => console.log(response),
      (errors) => console.log(errors),
      () => console.log('Request completed')
    );
  }

  public getServers() {
    this.serverService.getServers().subscribe(
      (servers: any[]) => this.servers = servers,
      // console.log(servers),
      // {
        // console.log(response);
        // const data = response.json();
        // console.log(data);
      // }
      (error) => {
        console.log(error);
      }
    );
  }
}
