import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
@Injectable()
export class ServerService {

  public constructor(private http: Http) {}

  public addServers(servers: any[]): Observable<any> {
    // const headers: Headers = new Headers({
    //   myheader: 'myownheader'
    // });
    // return this.http.post('https://learning-angular-1c064.firebaseio.com/data.json', servers, {
    //   headers: headers
    // });
    return this.http.put('https://learning-angular-1c064.firebaseio.com/data.json', servers);
  }

  public getServers() {
    return this.http.get('https://learning-angular-1c064.firebaseio.com/data')
            .map(
              (response: Response) => {
                const data = response.json();
                for (const server of data) {
                  server.name = 'Fetched.. ' + server.name;
                }
                return data;
              }
            )
            .catch(
              (error: Response) => {
                return Observable.throw('Something went wrong');
              }
            );
  }

  public getAppName() {
    return this.http.get('https://learning-angular-1c064.firebaseio.com/appName.json')
    .map(
      (response: Response) => {
        return response.json();
      }
    );
  }
}
