import { Component } from '@angular/core';
import { RecipeService } from '../recipes/recipe.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {


  public constructor(private recipeService: RecipeService) {}

  public onSaveData() {
    this.recipeService.saveRecipes().subscribe(
      (response: any) => console.log(response),
      (error: any) => console.log(error)
    );
  }

  public onFetchData() {
    this.recipeService.fetchData().subscribe(
      (recipes: any[]) => {
        console.log(recipes);
        this.recipeService.clearRecipes();
        this.recipeService.setRecipes(recipes);
      },
      // (error: any) => console.log(error)
    );
  }
}
